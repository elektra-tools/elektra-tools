#include "weyoume.hpp"
#include "genopt.hpp"

#include <ctime>
#include <random>
#include <iostream>

int main(int argc, char** argv)
{
	using namespace std;
	using namespace kdb;

	KDB kdb;
	KeySet ks;
	kdb.get(ks, "/sw/weyoume");
	if (ksGetOpt(argc, argv, ks.getKeySet()))
	{
		std::cout << "Usage " << argv[0] << " [OPTIONS]\n"
			<< elektraGenHelpText() << "\n"
			<< "Outputs the answer to the question if we, you and me should do the work in question"
			<< std::endl;
		return 1;
	}

	Parameters par(ks);

	std::random_device rd(par.getSwWeyoumeRandomDevice());

	if (par.getSwWeyoumeVerbose())
	{
		std::cout << "minimum: " << rd.min() << std::endl;
		std::cout << "maximum: " << rd.max() << std::endl;
		std::cout << "entropy: " << rd.entropy() << std::endl;
	}

	std::mt19937 engine(rd());

	switch (std::uniform_int_distribution<int>{0, 2}(engine))
	{
		case 0: cout << par.getSwWeyoumeNameWe() << endl;
			break;
		case 1: cout << par.getSwWeyoumeNameYou() << endl;
			break;
		case 2: cout << par.getSwWeyoumeNameMe() << endl;
			break;
	}
	return 0;
}
